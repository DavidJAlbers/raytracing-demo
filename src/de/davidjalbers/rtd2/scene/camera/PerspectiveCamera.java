/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene.camera;

import de.davidjalbers.rtd2.scene.Ray;
import de.davidjalbers.rtd2.scene.Vector3f;
import de.davidjalbers.rtd2.scene.generic.SceneObjectBase;

/**
 * Eine Kamera, die grundlegende perspektivische Verzerrung berücksichtigt. Weiter entfernte Objekte erscheinen kleiner, da weniger Strahlen auf sie treffen.
 * Achtung, experimentell!
 */
public class PerspectiveCamera extends SceneObjectBase implements ICamera {

    private final Vector3f planeCenterPos;

    /**
     * Erzeugt eine neue perspektivische Kamera mit einer Augenposition und einer Bildschirmposition.
     * @param eyePos die Augenposition der Kamera
     * @param planeCenterPos die Bildschirmposition der Kamera
     */
    public PerspectiveCamera(Vector3f eyePos, Vector3f planeCenterPos) {
        super(eyePos.x, eyePos.y, eyePos.z);
        this.planeCenterPos = planeCenterPos;
    }

    @Override
    public Ray generateRay(int x, int y, int width, int height) {
        x = x - (width / 2);
        y = y - (height / 2);

        Vector3f eyeCenterPos = pos; // alias für super-class member

        Vector3f direction = planeCenterPos.subtract(eyeCenterPos).normalize();

        Vector3f directionVectorX = direction
                .cross(new Vector3f(0, 1, 0))
                .normalize();
        Vector3f directionVectorY = direction
                .cross(directionVectorX)
                .normalize();

        return new Ray( eyeCenterPos, planeCenterPos
                .add(directionVectorX.multiply(x))
                .add(directionVectorY.multiply(y))
                .subtract(eyeCenterPos)
                .normalize());
    }

}
