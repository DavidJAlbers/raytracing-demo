/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene.camera;

import de.davidjalbers.rtd2.scene.Ray;

/**
 * Eine Schnittstelle für jede Kamera, die zum Raytracing verwendet werden soll.
 */
public interface ICamera {

    /**
     * Erzeugt unter Berücksichtung der Konfiguration dieser Kamera einen Strahl zur Bildsynthese eines bestimmten Pixels in einem Bild mit gegebenen Maßen.
     * @param x der x-Index des Pixels
     * @param y der y-Index des Pixels
     * @param width die Breite des Bildes in Pixeln
     * @param height die Höhe des Bildes in Pixeln
     * @return der fertig erzeugte Strahl
     */
    Ray generateRay(int x, int y, int width, int height);

}
