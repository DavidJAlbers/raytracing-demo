/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene.geometry;

import de.davidjalbers.rtd2.scene.Intersection;
import de.davidjalbers.rtd2.scene.Ray;
import de.davidjalbers.rtd2.scene.Vector3f;
import de.davidjalbers.rtd2.scene.generic.ColorableSceneObjectBase;

import java.awt.*;

/**
 * Repräsentiert eine Kugel im dreidimensionalen Raum.
 */
public class Sphere extends ColorableSceneObjectBase implements IGeometry {

    public final float rad;

    /**
     * Erzeugt eine neue Kugel mit einer Position, einem Radius und einer Farbe.
     * @param pos die Position der Kugel
     * @param rad der Radius der Kugel
     * @param col die Farbe der Kugel
     */
    public Sphere(Vector3f pos, float rad, Color col) {
        super(pos, col);
        this.rad = rad;
    }

    @Override
    public Intersection intersect(Ray ray) {
//        float a, b, c, D;
//        a = 1;
//        b = ray.dir.dot((ray.pos.subtract(pos).multiply(2)));
//        c = pos.dot(pos) + ray.pos.dot(ray.pos) - ray.pos.dot(pos) * 2 - rad * rad;
//        D = b * b - a * c * 4;
//
//        Intersection intersection = new Intersection();
//        intersection.happened = false;
//        if (D >= 0) {
//            float sqrt_D = (float) Math.sqrt(D);
//            float t = (float) ((-0.5) * (b + sqrt_D) / a);
//            if (t > 0) { // Kollision fand in Strahlrichtung statt
//                float distance = (float) (Math.sqrt(a) * t);
//                Vector3f hitpoint = ray.pos.add(ray.dir.multiply(t));
//                Vector3f normal = (hitpoint.subtract(pos)).divide(rad);
//
//                intersection.happened = true;
//                intersection.contactCoord = hitpoint;
//                intersection.normalUnitVec = normal;
//            }
//        }
//
//        return intersection.happened;

        Vector3f oc = ray.pos.subtract(pos);
        float a = Vector3f.dot(ray.dir, ray.dir);
        float b = 2.0f * Vector3f.dot(oc, ray.dir);
        float c = Vector3f.dot(oc, oc) - rad*rad;
        float discriminant = b*b - 4*a*c;

        Intersection intersection = new Intersection();
        intersection.happened = discriminant > 0;

        if (intersection.happened) {
            intersection.contactCoord = ray.pos.add(ray.dir.multiply((float) ((-0.5) * (b + Math.sqrt(discriminant)) / a)));
        }

        return intersection;
    }

    @Override
    public Color getPrimaryColor() {
        return super.col;
    }

}
