/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene;

/**
 * Repräsentiert einen Schnittpunkt.
 */
public class Intersection {

    public boolean happened;

    public Vector3f contactCoord;

    public Vector3f normalUnitVec;

}
