/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene;

import de.davidjalbers.rtd2.scene.camera.ICamera;
import de.davidjalbers.rtd2.scene.geometry.IGeometry;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Repräsentiert eine virtuelle szenische Umgebung mit einer Hintergrundfarbe, einer Kamera und verschiedenen geometrischen Objekten.
 */
public class Scene {

    public final Color ambient;

    public final ICamera camera;

    public final ArrayList<IGeometry> objects;

    /**
     * Erzeugt eine neue Szene aus gegebenen Parametern.
     * @param ambient die Hintergrundfarbe der zu erzeugenden Szene
     * @param camera die Kameraperspektive, von der die Szene gerendert werden soll
     * @param objects die geometrischen Objekte, die die Szene zu Beginn enthalten soll (später veränderbar)
     */
    public Scene(Color ambient, ICamera camera, ArrayList<IGeometry> objects) {
        this.ambient = ambient;
        this.camera = camera;
        this.objects = objects;
    }

    /**
     * Führt eine Bildsynthese für diese Szene durch.
     * @param width die Breite des zu erzeugenden Bildes in Pixeln
     * @param height die Höhe des zu erzeugenden Bildes in Pixeln
     * @return das fertig synthetisierte Bild
     */
    public BufferedImage render(int width, int height) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                Ray ray = camera.generateRay(x, y, width, height);
                img.setRGB(x, y, trace(ray).getRGB());
            }
        }

        return img;
    }

    private Color trace(Ray ray) {
        IGeometry hit = null;
        float distance = Float.MAX_VALUE;
        for (IGeometry object : objects) {
            Intersection intersection = object.intersect(ray);
            if (intersection.happened) {
                float currentDistance = intersection.contactCoord.subtract(ray.pos).magnitude();
                if (currentDistance < distance) {
                    distance = currentDistance;
                    hit = object;
                }
            }
        }
        if (hit != null)
            return hit.getPrimaryColor();
        return ambient;
    }
}
