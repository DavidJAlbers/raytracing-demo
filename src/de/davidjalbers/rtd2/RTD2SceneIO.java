/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2;

import de.davidjalbers.rtd2.scene.Scene;
import de.davidjalbers.rtd2.scene.Vector3f;
import de.davidjalbers.rtd2.scene.camera.ICamera;
import de.davidjalbers.rtd2.scene.camera.OrthograficCamera;
import de.davidjalbers.rtd2.scene.camera.PerspectiveCamera;
import de.davidjalbers.rtd2.scene.geometry.IGeometry;
import de.davidjalbers.rtd2.scene.geometry.Sphere;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Eine statische Klasse, die die Methode {@link #read(String)} zum Einlesen einer Szene bereitstellt.
 */
public final class RTD2SceneIO {

    private RTD2SceneIO() {}

    /**
     * Liest eine Szene aus einer Datei.
     * @param in Name der Datei, aus der gelesen werden soll
     * @return die fertig gelesene Szene
     */
    public static Scene read(String in) {
        // Defaults
        Color ambient = new Color(0, 0, 0);
        ICamera camera = new OrthograficCamera(new Vector3f(0.0f, 0.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), 1);
        ArrayList<IGeometry> objects = new ArrayList<>();

        try (Scanner reader = new Scanner(new File(in))) {
            lineloop:
            while (reader.hasNextLine()) {
                String line = reader.nextLine();

                // Kommentare und leere Zeilen ausschließen
                if (line.startsWith("//") || line.isBlank()) {
                    continue lineloop;
                }

                // Direktiven prüfen
                String[] components = line.split(":");
                if (components.length > 1) {
                    switch (components[0]) {
                        case "ambient":
                            ambient = readColor(components[1]);
                            continue lineloop;
                        case "orthografic_camera":
                            camera = new OrthograficCamera(readVector(components[1]), readVector(components[2]), readFloat(components[3]));
                            continue lineloop;
                        case "perspective_camera":
                            camera = new PerspectiveCamera(readVector(components[1]), readVector(components[2]));
                            continue lineloop;
                        case "sphere":
                            objects.add(new Sphere(readVector(components[1]), readFloat(components[2]), readColor(components[3])));
                            continue lineloop;
                    }
                } else {
                    throw new RuntimeException("invalid file format");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new Scene(ambient, camera, objects);
    }

    private static Color readColor(String in) {
        int r, g, b;
        Scanner reader = new Scanner(in).useDelimiter("[|()]");
        r = reader.nextInt();
        g = reader.nextInt();
        b = reader.nextInt();
        return new Color(r, g, b);
    }

    private static Vector3f readVector(String in) {
        float x, y, z;
        Scanner reader = new Scanner(in).useDelimiter("[|()]");
            x = reader.nextFloat();
            y = reader.nextFloat();
            z = reader.nextFloat();
        return new Vector3f(x, y, z);
    }

    private static float readFloat(String in) {
        float f;
        Scanner reader = new Scanner(in).useDelimiter("[|()]");
        f = reader.nextFloat();
        return f;
    }

}
