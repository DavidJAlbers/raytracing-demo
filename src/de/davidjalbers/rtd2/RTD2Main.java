/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2;

import de.davidjalbers.rtd2.scene.Scene;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Eine statische Klasse, die den Einstiegspunkt des Programms enthält.
 */
public final class RTD2Main {

    private RTD2Main() {}

    /**
     * Einstiegspunkt des Programms. Es wird zunächst die Eingabe gelesen, dann die Szene gerendert (in Full-HD-Auflösung) und die Ausgabe geschrieben.
     * @param args Befehlszeilenargumente (nicht verwendet)
     * @throws IOException zeigt eine fehlerhafte Eingabe an
     */
    public static void main(String[] args) throws IOException {
        Scene scene = RTD2SceneIO.read("input_rtd2.txt");
        ImageIO.write(
                scene.render(1000, 1000), "PNG", new File("output_rtd2.png")
        );
    }

}
